package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    int ind = 0;
    @Override
    public long sum(List <Long> list) {
        if (!list.isEmpty()) {
            if (ind != list.size()) {
                long num = list.get(ind);
                ind++;
                return num + sum(list);
            }
            else {
                ind = 0;
                return 0;
            }
        }
        else {
            return 0;
        }
    }
    

}

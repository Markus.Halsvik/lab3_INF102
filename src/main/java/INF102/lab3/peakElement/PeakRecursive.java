package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    int ind = 0;
    @Override
    public int peakElement(List <Integer> numbers) {
        if (!numbers.isEmpty()) {
            if (numbers.size() != 1) {
                if (0 < ind && ind < numbers.size() - 1) {      
                    if (numbers.get(ind - 1) <= numbers.get(ind) && numbers.get(ind) >= numbers.get(ind + 1)) {
                        int peak = numbers.get(ind);
                        ind = 0;
                        return peak;
                    }
                    else {
                        ind++;
                        return peakElement(numbers);
                    }
                }
                else {
                    if (ind == 0) {
                        if (numbers.get(0) >= numbers.get(1)) {
                            int peak = numbers.get(ind);
                            ind = 0;
                            return peak;
                        }
                        else {
                            ind++;
                            return peakElement(numbers);
                        }
                    }
                    else {
                        if (numbers.get(ind) >= numbers.get(ind - 1)) {
                            int peak = numbers.get(ind);
                            ind = 0;
                            return peak;
                        }
                    }
                }
            }
            else {
                int peak = numbers.get(ind);
                return peak;
            }
        }
        return 0;
    }
}

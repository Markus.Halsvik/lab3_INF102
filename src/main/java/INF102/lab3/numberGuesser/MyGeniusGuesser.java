package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int low = number.getLowerbound();
        int upp = number.getUpperbound();
        while (true) {
            int half = (upp + low) / 2;
            int test = number.guess(half);
            if (test == -1) {
                low = half;
            }
            if (test == 0) {
                return half;
            }
            if (test == 1) {
                upp = half;
            }
        }
    }
}
